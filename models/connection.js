let mysql = require('mysql')

let connection = mysql.createConnection({
    host : 'localhost',
    user : 'root',
    password : '',
    database : 'db_captchat'
})

connection.connect()

module.exports = connection