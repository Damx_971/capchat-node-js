let connection = require('./connection')

class Upload{

    static load(name, theme, indice, cb){
        connection.query('INSERT INTO captchat(nom, categorie, img1, img2, img3, img4, img5, img6, img7, img8, img9, imgSinguliere, indice) VALUES (?, ?, "img1", "img2", "img3", "img4", "img5", "img6", "img7", "img8", "img9", "imgSinguliere", ?)', 
        [name, theme, indice], (err, result)=>{
            if(err) throw err
            cb(result)

        })
    }

    static all (cb){
        connection.query('SELECT * FROM theme', (err, rows)=>{
            if(err) throw err
            cb(rows)

        })
    }
}

module.exports=Upload
